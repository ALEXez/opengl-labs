#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <math.h>
#include <gl/gl.h>
#include <gl/glu.h>

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
void EnableOpenGL(HWND hwnd, HDC*, HGLRC*);
void DisableOpenGL(HWND, HDC, HGLRC);
void InitCoords();
void RefreshCoords();

typedef struct {
    int first, second, third, fourth;
    float avgZ, nz;
} Poly;

Poly newPoly(int first, int second, int third, int fourth, float avgZ) {
    Poly temp;
    temp.first = first;
    temp.second = second;
    temp.third = third;
    temp.fourth = fourth;
    temp.avgZ = avgZ;
    return temp;
}

typedef struct {
    int first, second, third;
    float nz;
} Point;

Point newPoint(int first, int second, int third) {
    Point temp;
    temp.first = first;
    temp.second = second;
    temp.third = third;
    temp.nz = 0;
    return temp;
}

int X0[8], Y0[8], Z0[8];
float X1[8], Y1[8], Z1[8];
float X[8], Y[8];
Point points[8];
Poly polys[6];
Poly temp;
float theta = 0.3f, phi = 1.9f;
int d = 200, Ro = 500, ind = 0;
float velocity = 0.05f;
time_t t;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    WNDCLASSEX wcex;
    HWND hwnd;
    HDC hDC;
    HGLRC hRC;
    MSG msg;
    BOOL bQuit = FALSE;

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_OWNDC;
    wcex.lpfnWndProc = WindowProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = "GLSample";
    wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);


    if(!RegisterClassEx(&wcex))
        return 0;

    hwnd = CreateWindowEx(0,
                          "GLSample",
                          "������������ ������ �3",
                          WS_OVERLAPPEDWINDOW,
                          CW_USEDEFAULT,
                          CW_USEDEFAULT,
                          700,
                          700,
                          NULL,
                          NULL,
                          hInstance,
                          NULL);

    ShowWindow(hwnd, nCmdShow);

    InitCoords();
    temp = newPoly(0, 0, 0, 0, 0);
    polys[0] = newPoly(1, 0, 2, 3, 0);
    polys[1] = newPoly(6, 5, 2, 1, 0);
    polys[2] = newPoly(7, 4, 6, 5, 0);
    polys[3] = newPoly(3, 0, 7, 4, 0);
    polys[4] = newPoly(0, 1, 4, 5, 0);
    polys[5] = newPoly(3, 7, 2, 6, 0);
    ind = 6;
    points[0] = newPoint(0, 3, 4);
    points[1] = newPoint(0, 1, 4);
    points[2] = newPoint(0, 1, 5);
    points[3] = newPoint(0, 3, 5);
    points[4] = newPoint(2, 3, 4);
    points[5] = newPoint(1, 2, 4);
    points[6] = newPoint(1, 2, 5);
    points[7] = newPoint(2, 3, 5);
    EnableOpenGL(hwnd, &hDC, &hRC);
    /*GLuint texture;
    unsigned char header[54];
    unsigned char * data;
    FILE * file;
    file = fopen("back.bmp", "rb");
    if(file == NULL) return 0;
    data = (unsigned char *)malloc(750000);
    fread(header, 1, 54, file);
    fread(data, 750000, 1, file);
    fclose(file);
    int i;
    int temp;
    for(i = 0; i < 750000; i += 3) {
        temp = data[i];
        data[i] = data[i + 2];
        data[i + 2] = temp;
    }
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);


    //even better quality, but this will do for now.
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


    //to the edge of our shape.
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    //Generate the texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 500, 500, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    free(data);*/

    static LARGE_INTEGER CounterFrequency;
    static LARGE_INTEGER FPSCount;
    QueryPerformanceFrequency(&CounterFrequency);
    QueryPerformanceCounter(&FPSCount);
    while(!bQuit) {
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
            if(msg.message == WM_QUIT) {
                bQuit = TRUE;
            } else {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        } else {
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            glClear(GL_COLOR_BUFFER_BIT);

            RefreshCoords();

            glPushMatrix();
            glColor3d(1, 1, 1);
            /*glEnable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
            glTexCoord2d(0.0, 0.0);
            glVertex2f(-1, -1);
            glTexCoord2d(1.0, 0.0);
            glVertex2f(1, -1);
            glTexCoord2d(1.0, 1.0);
            glVertex2f(1, 1);
            glTexCoord2d(0.0, 1.0);
            glVertex2f(-1, 1);
            glEnd();*/
            glLineWidth(3);
            glEnable(GL_LINE_SMOOTH);
            glBegin(GL_LINE_STRIP);
            glColor3d(1, 1, 1);
            int i;
            for(i = 0; i < 4; i++) {
                glVertex2d(X[i], Y[i]);
            }
            glVertex2d(X[0], Y[0]);
            glEnd();
            glBegin(GL_LINE_STRIP);
            for(i = 4; i < 8; i++) {
                glVertex2d(X[i], Y[i]);
            }
            glVertex2d(X[4], Y[4]);
            glEnd();
            glBegin(GL_LINES);
            glVertex2d(X[0], Y[0]);
            glVertex2d(X[4], Y[4]);
            glVertex2d(X[1], Y[1]);
            glVertex2d(X[5], Y[5]);
            glVertex2d(X[2], Y[2]);
            glVertex2d(X[6], Y[6]);
            glVertex2d(X[3], Y[3]);
            glVertex2d(X[7], Y[7]);
            glEnd();
            glColor3d(0, 1, 0);
            for(i = 0; i < 6; i++) {
                if(polys[i].nz < 0) continue;
                float first = (points[polys[i].first].nz + 1) / 2;
                float second = (points[polys[i].second].nz + 1) / 2;
                float third = (points[polys[i].third].nz + 1) / 2;
                float fourth = (points[polys[i].fourth].nz + 1) / 2;
                glBegin(GL_TRIANGLE_STRIP);
                glColor3d(first, first, first);
                glVertex2d(X[polys[i].first], Y[polys[i].first]);
                glColor3d(second, second, second);
                glVertex2d(X[polys[i].second], Y[polys[i].second]);
                glColor3d(third, third, third);
                glVertex2d(X[polys[i].third], Y[polys[i].third]);
                glColor3d(fourth, fourth, fourth);
                glVertex2d(X[polys[i].fourth], Y[polys[i].fourth]);
                glEnd();
            }
            glDisable(GL_LINE_SMOOTH);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glPopMatrix();
            SwapBuffers(hDC);

            static int iFrames = 0;
            static float fps = 0.0f;
            iFrames++;
            if(iFrames == 50) {
                float fTime;
                LARGE_INTEGER lCurrent;
                QueryPerformanceCounter(&lCurrent);
                fTime = (float)(lCurrent.QuadPart - FPSCount.QuadPart) / (float)CounterFrequency.QuadPart;
                fps = (float)iFrames / fTime;
                iFrames = 0;
                QueryPerformanceCounter(&FPSCount);
            }
            char buf2[32];
            sprintf(buf2, "������������ ������ �3. FPS: %4.0f", fps);
            SetWindowText(hwnd, buf2);
            //Sleep(1);
        }
    }
    DisableOpenGL(hwnd, hDC, hRC);
    DestroyWindow(hwnd);
    return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch(uMsg) {
        case WM_CLOSE:
            PostQuitMessage(0);
            break;

        case WM_DESTROY:
            return 0;

        case WM_KEYDOWN: {
                switch(wParam) {
                    case VK_ESCAPE:
                        PostQuitMessage(0);
                        break;
                    case VK_UP:
                        phi += velocity;
                        if(phi > 2 * M_PI) phi -= 2 * M_PI;
                        break;
                    case VK_DOWN:
                        phi -= velocity;
                        if(phi < 0) phi += 2 * M_PI;
                        break;
                    case VK_LEFT:
                        theta -= velocity;
                        if(theta < 0) theta += 2 * M_PI;
                        break;
                    case VK_RIGHT:
                        theta += velocity;
                        if(theta > 2 * M_PI) theta -= 2 * M_PI;
                        break;
                }
            }
            break;

        default:
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    return 0;
}

void EnableOpenGL(HWND hwnd, HDC* hDC, HGLRC* hRC) {
    PIXELFORMATDESCRIPTOR pfd;
    int iFormat;
    *hDC = GetDC(hwnd);
    ZeroMemory(&pfd, sizeof(pfd));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;
    iFormat = ChoosePixelFormat(*hDC, &pfd);
    SetPixelFormat(*hDC, iFormat, &pfd);
    *hRC = wglCreateContext(*hDC);
    wglMakeCurrent(*hDC, *hRC);
}

void DisableOpenGL(HWND hwnd, HDC hDC, HGLRC hRC) {
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(hwnd, hDC);
}

void InitCoords() {
    X0[0] = X0[3] = X0[4] = X0[7] = -100;
    X0[1] = X0[2] = X0[5] = X0[6] = 100;

    Y0[0] = Y0[1] = Y0[4] = Y0[5] = -50;
    Y0[2] = Y0[3] = Y0[6] = Y0[7] = 50;

    Z0[0] = Z0[1] = Z0[2] = Z0[3] = -50;
    Z0[4] = Z0[5] = Z0[6] = Z0[7] = 50;
}

void RefreshCoords() {
    int i, ins, j, k = 57;
    for(i = 0; i < 8; i++) {
        X1[i] = X0[i] * (-sin(theta)) + Y0[i] * (cos(theta));
        Y1[i] = X0[i] * (-cos(phi) * cos(theta)) - Y0[i] * (cos(phi) * sin(theta)) + Z0[i] * (sin(phi));
        Z1[i] = X0[i] * (-sin(phi) * cos(theta)) - Y0[i] * (sin(phi) * sin(theta)) - Z0[i] * (cos(phi)) + Ro;
        X[i] = d * X1[i] / Z1[i] / 100;
        Y[i] = d * Y1[i] / Z1[i] / 100;
        points[i].nz = (polys[points[i].first].nz + polys[points[i].second].nz + polys[points[i].third].nz) / 3;
    }
    for(i = 0; i < ind; i++) {
        double   nx = Y1[polys[i].first] * (Z1[polys[i].second] - Z1[polys[i].third]) + Y1[polys[i].second] * (Z1[polys[i].third] - Z1[polys[i].first]) + Y1[polys[i].third] * (Z1[polys[i].first] - Z1[polys[i].second]);
        double   ny = Z1[polys[i].first] * (X1[polys[i].second] - X1[polys[i].third]) + Z1[polys[i].second] * (X1[polys[i].third] - X1[polys[i].first]) + Z1[polys[i].third] * (X1[polys[i].first] - X1[polys[i].second]);
        polys[i].nz = X1[polys[i].first] * (Y1[polys[i].second] - Y1[polys[i].third]) + X1[polys[i].second] * (Y1[polys[i].third] - Y1[polys[i].first]) + X1[polys[i].third] * (Y1[polys[i].first] - Y1[polys[i].second]);
        double length = sqrt(nx * nx + ny * ny + polys[i].nz * polys[i].nz);
        polys[i].nz /= length;

        polys[i].avgZ = (Z1[polys[i].first] + Z1[polys[i].second] + Z1[polys[i].third] + Z1[polys[i].fourth]) / 4;
        for(j = i; j < ind; j++) {
            if(polys[j].avgZ > polys[i].avgZ) {
                temp.first = polys[j].first;
                temp.second = polys[j].second;
                temp.third = polys[j].third;
                temp.fourth = polys[j].fourth;
                polys[j].first = polys[i].first;
                polys[j].second = polys[i].second;
                polys[j].third = polys[i].third;
                polys[j].fourth = polys[i].fourth;
                polys[i].first = temp.first;
                polys[i].second = temp.second;
                polys[i].third = temp.third;
                polys[i].fourth = temp.fourth;
                for(ins = 0; ins < 8; ins++) {
                    if(points[ins].first == i) points[ins].first = j;
                    else if(points[ins].first == j) points[ins].first = i;
                    if(points[ins].second == i) points[ins].second = j;
                    else if(points[ins].second == j) points[ins].second = i;
                    if(points[ins].third == i) points[ins].third = j;
                    else if(points[ins].third == j) points[ins].third = i;
                }
            }
        }
    }
}
